# -*- coding: utf-8 -*-
#Библиотеки, които използват python и Kodi в тази приставка
import re
import sys
import os
import time
import urllib
import urllib2
import json
from hashlib import sha1
import hmac
import binascii
import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon




#Място за дефиниране на константи, които ще се използват няколкократно из отделните модули
__addon_id__= 'plugin.video.vikir'
__Addon = xbmcaddon.Addon(__addon_id__)
__settings__ = xbmcaddon.Addon(id='plugin.video.vikir')

#Настройка качеството на видеото
quality =__settings__.getSetting('quality')


#Поддръжка на Фен канали
fc =__settings__.getSetting('fc')

#Дебъг режим
debug =__settings__.getSetting('debug')


#Настройка реда на епизодите
d =__settings__.getSetting('direction')
if d =='0':
	d='desc'
elif d =='1':
	d='asc'

#Настройка езика на субтитрите
se =__settings__.getSetting('se')
sublang =__settings__.getSetting('lang')
if sublang =='0':
	lang='en'
	language = 'English'
elif sublang =='1':
	lang='bg'
	language = 'Bulgarian'
elif sublang =='2':
	lang='es'
	language = 'Spanish'
elif sublang =='3':
	lang='fr'
	language = 'French'
elif sublang =='4':
	lang='pt'
	language = 'Portuguese'
elif sublang =='5':
	lang='ja'
	language = 'Japanese'
elif sublang =='6':
	lang='zh-cn'
	language = 'Chinese'
elif sublang =='7':
	lang='zh-tw'
	language = 'Chinese'
elif sublang =='8':
	lang='ko'
	language = 'Korean'
elif sublang =='9':
	lang='ab'
	language = 'Abkhazian'
elif sublang =='10':
	lang='aa'
	language = 'Afar'
elif sublang =='11':
	lang='af'
	language = 'Afrikaans'
elif sublang =='12':
	lang='ak'
	language = 'Akana'
elif sublang =='13':
	lang='am'
	language = 'Amharic'
elif sublang =='14':
	lang='ag'
	language = 'English'
elif sublang =='15':
	lang='az'
	language = 'Azerbaijani'
elif sublang =='16':
	lang='ms'
	language = 'Malay'
elif sublang =='17':
	lang='id'
	language = 'Indonesian'
elif sublang =='18':
	lang='jv'
	language = 'Basa Jawa'
elif sublang =='19':
	lang='bn'
	language = 'Bengali'
elif sublang =='20':
	lang='bo'
	language = 'Tibetan'
elif sublang =='21':
	lang='bs'
	language = 'Bosanski'
elif sublang =='22':
	lang='ca'
	language = 'Catalan'
elif sublang =='23':
	lang='ch'
	language = 'Chamoru'
elif sublang =='24':
	lang='ce'
	language = 'Cherokee'
elif sublang =='25':
	lang='za'
	language = 'Cuengh'
elif sublang =='26':
	lang='cy'
	language = 'Welsh'
elif sublang =='27':
	lang='da'
	language = 'Danish'
elif sublang =='28':
	lang='de'
	language = 'German'
elif sublang =='29':
	lang='dv'
	language = 'Divehi'
elif sublang =='30':
	lang='dz'
	language = 'Bhutani'
elif sublang =='31':
	lang='et'
	language = 'Estonian'
elif sublang =='32':
	lang='eo'
	language = 'Esperanto'
elif sublang =='33':
	lang='eu'
	language = 'Basque'
elif sublang =='34':
	lang='to'
	language = 'Tonga'
elif sublang =='35':
	lang='ga'
	language = 'Irish'
elif sublang =='36':
	lang='sm'
	language = 'Samoan'
elif sublang =='37':
	lang='gl'
	language = 'Galician'
elif sublang =='38':
	lang='gd'
	language = 'Scots Gaelic'
elif sublang =='39':
	lang='hm'
	language = 'Hmong'
elif sublang =='40':
	lang='hr'
	language = 'Croatian'
elif sublang =='41':
	lang='ia'
	language = 'Interlingua'
elif sublang =='42':
	lang='it'
	language = 'Italian'
elif sublang =='43':
	lang='mu'
	language = 'Karaoke'
elif sublang =='44':
	lang='cb'
	language = 'Kaszëbsczi'
elif sublang =='45':
	lang='kw'
	language = 'Kernewek/Karnuack'
elif sublang =='46':
	lang='km'
	language = 'Cambodian'
elif sublang =='47':
	lang='rn'
	language = 'Kirundi'
elif sublang =='48':
	lang='sw'
	language = 'Swahili'
elif sublang =='49':
	lang='hat'
	language = 'Kreyòlayisyen'
elif sublang =='50':
	lang='ku'
	language = 'Kurdish'
elif sublang =='51':
	lang='lo'
	language = 'Laothian'
elif sublang =='52':
	lang='la'
	language = 'Latin'
elif sublang =='53':
	lang='lv'
	language = 'Latvian'
elif sublang =='54':
	lang='lt'
	language = 'Lithuanian'
elif sublang =='55':
	lang='hu'
	language = 'Hungarian'
elif sublang =='56':
	lang='mg'
	language = 'Malagasy'
elif sublang =='57':
	lang='ml'
	language = 'Malayalam'
elif sublang =='58':
	lang='mo'
	language = 'Moldavian'
elif sublang =='59':
	lang='my'
	language = 'Burmese'
elif sublang =='60':
	lang='fj'
	language = 'Fiji'
elif sublang =='61':
	lang='nl'
	language = 'Dutch'
elif sublang =='62':
	lang='cr'
	language = 'Nehiyaw'
elif sublang =='63':
	lang='no'
	language = 'Norwegian'
elif sublang =='64':
	lang='or'
	language = 'Oriya'
elif sublang =='65':
	lang='uz'
	language = 'Uzbek'
elif sublang =='66':
	lang='pl'
	language = 'Polish'
elif sublang =='67':
	lang='ro'
	language = 'Romanian'
elif sublang =='68':
	lang='rm'
	language = 'Rhaeto-Romance'
elif sublang =='69':
	lang='st'
	language = 'Sesotho'
elif sublang =='70':
	lang='sq'
	language = 'Albanian'
elif sublang =='71':
	lang='sk'
	language = 'Slovak'
elif sublang =='72':
	lang='sl'
	language = 'Slovenian'
elif sublang =='73':
	lang='so'
	language = 'Somali'
elif sublang =='74':
	lang='sh'
	language = 'Serbo-Croatian'
elif sublang =='75':
	lang='fi'
	language = 'Finnish'
elif sublang =='76':
	lang='sv'
	language = 'Swedish'
elif sublang =='77':
	lang='tl'
	language = 'Tagalog'
elif sublang =='78':
	lang='tt'
	language = 'Tatar'
elif sublang =='79':
	lang='vi'
	language = 'Vietnamese'
elif sublang =='80':
	lang='tw'
	language = 'Twi'
elif sublang =='81':
	lang='tr'
	language = 'Turkish'
elif sublang =='82':
	lang='wo'
	language = 'Wolof'
elif sublang =='83':
	lang='yo'
	language = 'Yoruba'
elif sublang =='84':
	lang='sn'
	language = 'Shona'
elif sublang =='85':
	lang='lol'
	language = 'lolspeak'
elif sublang =='86':
	lang='tm'
	language = 'tlh Ingan-Hol'
elif sublang =='87':
	lang='is'
	language = 'Icelandic'
elif sublang =='88':
	lang='cs'
	language = 'Czech'
elif sublang =='89':
	lang='el'
	language = 'Greek'
elif sublang =='90':
	lang='ba'
	language = 'Bashkir'
elif sublang =='91':
	lang='mk'
	language = 'Macedonian'
elif sublang =='92':
	lang='mn'
	language = 'Mongolian'
elif sublang =='93':
	lang='ru'
	language = 'Russian'
elif sublang =='94':
	lang='sr'
	language = 'Serbian'
elif sublang =='95':
	lang='uk'
	language = 'Ukrainian'
elif sublang =='96':
	lang='mne'
	language = 'црногорски'
elif sublang =='97':
	lang='kk'
	language = 'Kazakh'
elif sublang =='98':
	lang='hy'
	language = 'Armenian'
elif sublang =='99':
	lang='he'
	language = 'Hebrew'
elif sublang =='100':
	lang='ar'
	language = 'Arabic'
elif sublang =='101':
	lang='ur'
	language = 'Urdu'
elif sublang =='102':
	lang='skr'
	language = 'سرائیکی'
elif sublang =='103':
	lang='fa'
	language = 'Persian'
elif sublang =='104':
	lang='hne'
	language = 'छत्तीसगढ़ी'
elif sublang =='105':
	lang='ne'
	language = 'Nepali'
elif sublang =='106':
	lang='mr'
	language = 'Marathi'
elif sublang =='107':
	lang='sa'
	language = 'Sanskrit'
elif sublang =='108':
	lang='hi'
	language = 'Hindi'
elif sublang =='109':
	lang='pa'
	language = 'Punjabi'
elif sublang =='110':
	lang='gu'
	language = 'Gujarati'
elif sublang =='111':
	lang='ta'
	language = 'Tamil'
elif sublang =='112':
	lang='te'
	language = 'Tegulu'
elif sublang =='113':
	lang='kn'
	language = 'ಕನ್ನಡ'
elif sublang =='114':
	lang='th'
	language = 'Thai'
elif sublang =='115':
	lang='yue'
	language = '粵語'

else:
	lang='en'
	language = 'English'



#Деклариране на константи
md = xbmc.translatePath(__Addon.getAddonInfo('path') + "/resources/media/")
MUA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/ 604.1.21 (KHTML, like Gecko) Version/ 12.0 Mobile/17A6278a Safari/602.1.26' #За симулиране на заявка от мобилно устройство
UA = 'Mozilla/5.0 (Macintosh; MacOS X10_14_3; rv;67.0) Gecko/20100101 Firefox/67.0' #За симулиране на заявка от  компютърен браузър


#Меню с директории в приставката
def CATEGORIES():
		addDir('Search','https://api.viki.io/v4/search.json?page=1&per_page=50&app=100000a&term=','',3,md+'DefaultAddonsSearch.png')
		addLink('Play video by ID','loadbyid','0','True','','','G','5.0',8,md+'DefaultStudios.png')
		addDir('Browse Movies by Genre','movies','',6,md+'DefaultFolder.png')
		addDir('Browse Movies by Country','movies','',7,md+'DefaultFolder.png')
		addDir('New Movies','https://api.viki.io/v4/movies.json?sort=newest_video&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Recent Movies','https://api.viki.io/v4/movies.json?sort=views_recent&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Popular Movies','https://api.viki.io/v4/movies.json?sort=trending&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Best Movies','https://api.viki.io/v4/movies.json?sort=views&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Browse Series by Genre','series','',6,md+'DefaultFolder.png')
		addDir('Browse Series by Country','series','',7,md+'DefaultFolder.png')
		addDir('New Series','https://api.viki.io/v4/series.json?sort=newest_video&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Recent Series','https://api.viki.io/v4/series.json?sort=views_recent&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Popular Series','https://api.viki.io/v4/series.json?sort=trending&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Best Series','https://api.viki.io/v4/series.json?sort=views&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		#addDir('Latest News','https://api.viki.io/v4/news_clips.json?sort=newest_video&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		addDir('Latest Clips','https://api.viki.io/v4/clips.json?sort=newest_video&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		#if fc == 'true':
			#addDir('Music Videos','https://api.viki.io/v4/music_videos.json?sort=newest_video&page=1&per_page=50&app=100000a&t=','',1,md+'DefaultFolder.png')
		#addDir('Папка','https://www.example.com','',1,md+'DefaultFolder.png')




#Разлистване заглавията на подадената страница
def INDEX(url):
		timestamp = str(int(time.time()))
		#print url
		if 'search.json' in url:
			req = urllib2.Request(url)
		else:
			req = urllib2.Request(url+timestamp)
		req.add_header('User-Agent', UA)
		opener = urllib2.build_opener()
		f = opener.open(req)
		jsonrsp = json.loads(f.read())
		#print jsonrsp['response'][0]['titles']['en']
		
		#Начало на обхождането
		for movie in range(0, len(jsonrsp['response'])):
			try:
				if (jsonrsp['response'][movie]['flags']['licensed'] == True or fc == 'true' or debug == 'true'): #Ако заглавието е лицензирано или са разрешени Фен каналите/дебъг режима
					if jsonrsp['response'][movie]['type'] == 'series': #Ако е сериал
						xbmcplugin.setContent(int(sys.argv[1]), 'season')
						addDir(jsonrsp['response'][movie]['titles']['en'].encode('utf-8', 'ignore'),'https://api.viki.io/v4/series/'+jsonrsp['response'][movie]['id']+'/episodes.json?page=1&per_page=50&app=100000a&t='+timestamp,jsonrsp['response'][movie]['descriptions']['en'].encode('utf-8', 'ignore'),2,jsonrsp['response'][movie]['images']['poster']['url'])
					else: #Ако е игрален филм или клип
						if (jsonrsp['response'][movie]['blocked'] == False or debug == 'true'): #Проверка за достъпност
							try: #Субтитри на базовия език
								subtitle_completion1 = '0'
								subtitle_completion1 = str(jsonrsp['response'][movie]['subtitle_completions'][lang])
							except:
								pass
							try: #Субтитри на английски език
								subtitle_completion2 = '0'
								subtitle_completion2 = str(jsonrsp['response'][movie]['subtitle_completions']['en'])
							except:
								pass
							try: #Продължителност на видеото
								dur = ''
								dur = str(jsonrsp['response'][movie]['duration'])
							except:
								pass
							try: #Резолюция на видеото
								hd = 'False'
								hd = str(jsonrsp['response'][movie]['flags']['hd'])
							except:
								pass
							try: #Име/описание на филма
								mt = ''
								mt = jsonrsp['response'][movie]['titles']['en']
							except:
								pass
							try: #Автор/Студио
								at = ''
								at = jsonrsp['response'][movie]['author']
							except:
								pass
							try: #Рейтинг
								rating = 'G'
								rating = jsonrsp['response'][movie]['rating']
							except:
								pass
							try: #Оценка
								ar = '0'
								ar = str(jsonrsp['response'][movie]['container']['review_stats']['average_rating'])
							except:
								pass
							xbmcplugin.setContent(int(sys.argv[1]), 'movie')
							addLink(jsonrsp['response'][movie]['titles']['en'],jsonrsp['response'][movie]['id']+'@'+jsonrsp['response'][movie]['images']['poster']['url']+'@'+subtitle_completion1+'@'+subtitle_completion2+'@'+mt,dur,hd,mt,at,rating,ar,4,jsonrsp['response'][movie]['images']['poster']['url'])
							#addLink(jsonrsp['response'][movie]['titles']['en'],jsonrsp['response'][movie]['id']+'@'+jsonrsp['response'][movie]['images']['poster']['url']+'@'+subtitle_completion1+'@'+subtitle_completion2,jsonrsp['response'][movie]['duration'],hd,mt,at,rating,ar,4,jsonrsp['response'][movie]['images']['poster']['url'])
			except:
				pass
		#Край на обхождането
		
		#Ако имаме още страници...
		if jsonrsp['more'] == True:
			getpage=re.compile('(.+?)&page=(.+?)&per_page=(.+?)&t=').findall(url)
			for fronturl,page, backurl in getpage:
				newpage = int(page)+1
				url = fronturl + '&page=' + str(newpage) + '&per_page=' + backurl + '&t='
				#print 'URL OF THE NEXT PAGE IS' + url
				addDir('Next page >>',url,'',1,md+'DefaultFolder.png')
			
			
		





#Разлистване епизодите на сериала
def PREPARE(url):
		xbmcplugin.setContent(int(sys.argv[1]), 'episode')
		timestamp = str(int(time.time()))
		#print url
		req = urllib2.Request(url+'&direction='+d) #Задаване реда на епизодите
		req.add_header('User-Agent', UA)
		opener = urllib2.build_opener()
		f = opener.open(req)
		jsonrsp = json.loads(f.read())
		#print jsonrsp
		#print name+' has '+str(jsonrsp['response'][0]['number'])+' episodes'
		#jsonrsp['response'][episode]['rating']
		#jsonrsp['response'][episode]['author']
		#jsonrsp['response'][episode]['titles']['en']
		#jsonrsp['response'][episode]['container']['review_stats']['average_rating']
		
		#Начало на обхождането
		for episode in range(0, len(jsonrsp['response'])):
			try:
				if (jsonrsp['response'][episode]['blocked'] == False or debug == 'true'): #Проверка за достъпност - блокирано или не
					try: #Субтитри на базовия език
						subtitle_completion1 = '0'
						subtitle_completion1 = str(jsonrsp['response'][episode]['subtitle_completions'][lang])
					except:
						pass
					try: #Субтитри на английски език
						subtitle_completion2 = '0'
						subtitle_completion2 = str(jsonrsp['response'][episode]['subtitle_completions']['en'])
					except:
						pass
					try: #Резолюция на видеото
						hd = 'False'
						hd = str(jsonrsp['response'][episode]['flags']['hd'])
					except:
						pass
					try: #Име/описание на епизода
						et = ''
						et = jsonrsp['response'][episode]['titles']['en']
					except:
						pass
					try: #Автор/Студио
						at = ''
						at = jsonrsp['response'][episode]['author']
					except:
						pass
					try: #Рейтинг
						rating = 'G'
						rating = jsonrsp['response'][episode]['rating']
					except:
						pass
					addLink(jsonrsp['response'][episode]['container']['titles']['en'].encode('utf-8', 'ignore')+' Episode '+str(jsonrsp['response'][episode]['number']),jsonrsp['response'][episode]['id']+'@'+jsonrsp['response'][episode]['images']['poster']['url']+'@'+subtitle_completion1+'@'+subtitle_completion2+'@'+et,jsonrsp['response'][episode]['duration'],hd,et,at,rating,str(jsonrsp['response'][episode]['container']['review_stats']['average_rating']),4,jsonrsp['response'][episode]['images']['poster']['url'])
					#addLink(jsonrsp['response'][episode]['container']['titles']['en']+' Episode '+str(jsonrsp['response'][episode]['number']),jsonrsp['response'][episode]['id']+'@'+jsonrsp['response'][episode]['images']['poster']['url']+'@'+subtitle_completion1+'@'+subtitle_completion2,jsonrsp['response'][episode]['duration'],hd,et,at,rating,str(jsonrsp['response'][episode]['container']['review_stats']['average_rating']),4,jsonrsp['response'][episode]['images']['poster']['url'])
			except:
				pass
		if len(jsonrsp['response'])==0:
			addDir('There are no episodes for now','','','',md+'DefaultFolderBack.png')
		#Край на обхождането
		
		#Ако имаме още страници...
		if jsonrsp['more'] == True:
			getpage=re.compile('(.+?)page=(.+?)&per_page').findall(url)
			for fronturl,page in getpage:
				newpage = int(page)+1
				url = fronturl + 'page=' + str(newpage) + '&per_page=50&app=100000a&t=' + timestamp
				#print 'URL OF THE NEXT PAGE IS' + url
				addDir('Next page >>',url,'',2,md+'DefaultFolder.png')







#Разлистване по жанр
def GENRE(url):
		xbmcplugin.setContent(int(sys.argv[1]), 'season')
		req = urllib2.Request('https://api.viki.io/v4/videos/genres.json?app=100000a')
		req.add_header('User-Agent', UA)
		opener = urllib2.build_opener()
		f = opener.open(req)
		jsonrsp = json.loads(f.read())
		#print jsonrsp[0]['name']['en']
		
		#Начало на обхождането
		for genre in range(0, len(jsonrsp)):
			addDir(jsonrsp[genre]['name']['en'],'https://api.viki.io/v4/'+url+'.json?sort=newest_video&page=1&per_page=50&app=100000a&genre='+jsonrsp[genre]['id']+'&t=','',1,md+'DefaultFolder.png')
		#Край на обхождането









#Разлистване по държава
def COUNTRY(url):
		xbmcplugin.setContent(int(sys.argv[1]), 'season')
		req = urllib2.Request('https://api.viki.io/v4/videos/countries.json?app=100000a')
		req.add_header('User-Agent', UA)
		opener = urllib2.build_opener()
		f = opener.open(req)
		jsonrsp = json.loads(f.read())
		#print jsonrsp['ae']['name']['en']
		
		#Начало на обхождането
		for country, subdict in jsonrsp.iteritems():
			addDir(jsonrsp[country]['name']['en'],'https://api.viki.io/v4/'+url+'.json?sort=newest_video&page=1&per_page=50&app=100000a&origin_country='+country+'&t=','',1,md+'DefaultFolder.png')
		#Край на обхождането








#Търсачка
def SEARCH(url):
		xbmcplugin.setContent(int(sys.argv[1]), 'season')
		keyb = xbmc.Keyboard('', 'Search in VIKI® Database')
		keyb.doModal()
		searchText = ''
		if (keyb.isConfirmed()):
			searchText = urllib.quote_plus(keyb.getText())
			searchText=searchText.replace(' ','+')
			searchurl = url + searchText
			searchurl = searchurl.encode('utf-8')
			#print 'SEARCHING:' + searchurl
			INDEX(searchurl)
		else:
			addDir('Go to main menu...','','','',md+'DefaultFolderBack.png')







#Зареждане на клип по неговото ID
def LOADBYID():
		keyb = xbmc.Keyboard('', 'Enter video ID ...videos/xxxxxxv only')
		keyb.doModal()
		if (keyb.isConfirmed()):
			vid = urllib.quote_plus(keyb.getText())
			#print 'LOADBYID:' + vid
			PLAY('VIKI®',vid+'@0@50',md+'DefaultStudios.png')
		else:
			addDir('Go to main menu...','','','',md+'DefaultFolderBack.png')









#Подписване на заявки от името на Flash player
def SIGN(url,pth):
		#timestamp = str(int(time.time()))
		#key = '-$iJ}@p7!G@SyU/je1bEyWg}upLu-6V6-Lg9VD(]siH,r.,m-r|ulZ,U4LC/SeR)'
		#rawtxt = '/v4/videos/'+url+pth+'?app=65535a&t='+timestamp+'&site=www.viki.com'
		#hashed = hmac.new(key, rawtxt, sha1)
		#fullurl = 'https://api.viki.io' + rawtxt+'&sig='+binascii.hexlify(hashed.digest())
		#return fullurl
		
		timestamp = str(int(time.time()))
		key = 'MM_d*yP@`&1@]@!AVrXf_o-HVEnoTnm$O-ti4[G~$JDI/Dc-&piU&z&5.;:}95=Iad'
		rawtxt = '/v4/videos/'+url+pth+'?app=100005a&t='+timestamp+'&site=www.viki.com'
		hashed = hmac.new(key, rawtxt, sha1)
		fullurl = 'https://api.viki.io' + rawtxt+'&sig='+binascii.hexlify(hashed.digest())
		return fullurl










#Зареждане на видео и субтитри
def PLAY(name,url,iconimage):
		
		url, thumbnail, subtitle_completion1, subtitle_completion2, plot = url.split("@")#url, thumbnail, subtitle_completion1, subtitle_completion2 = url.split("@")

		#Заявка за получаване на субтитрите
		try:
			if (int(subtitle_completion1)>79 and se=='true'): #Ако имаме преведени над 79% субтитри на нашия език
				srtsubs_path = xbmc.translatePath('special://temp/vikir.'+language+'.srt')
				xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('VIKI®',language+' subtitles at '+subtitle_completion1+'%', 4000, md+'DefaultAddonSubtitles.png'))
				req = urllib2.Request(SIGN(url,'/subtitles/'+lang+'.srt'))
				req.add_header('User-Agent', UA)
				response = urllib2.urlopen(req)
				data=response.read()
				response.close()
				with open(srtsubs_path, "w") as subfile:
					subfile.write(data)
					sub = 'true'
			elif (int(subtitle_completion2)>0 and se=='true'): #Превключваме към английски субтитри
				srtsubs_path = xbmc.translatePath('special://temp/vikir.English.srt')
				xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('VIKI®','English subtitles at '+subtitle_completion2+'%', 4000, md+'DefaultAddonSubtitles.png'))
				req = urllib2.Request(SIGN(url,'/subtitles/en.srt'))
				req.add_header('User-Agent', UA)
				response = urllib2.urlopen(req)
				data=response.read()
				response.close()
				with open(srtsubs_path, "w") as subfile:
					subfile.write(data)
					sub = 'true'
			else:
				sub = 'false' #Ако няма налични субтитри
				xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%('VIKI®','Subtitles not available or disabled', 4000, md+'DefaultAddonSubtitles.png'))
		except:
			pass
		
		rapi = 'true'
		#Заявка за получаване на видео стрийма
		#request_headers = {
		#"Host": "www.viki.com",
		#"User-Agent": UA,
		#"Accept": "text/html, */*; q=0.01",
		#"Accept-Language": "en-US,en;q=0.5",
		#"Referer": "https://www.viki.com/player/",
		#"Connection": "keep-alive"
		#}
		#req = urllib2.Request('https://www.viki.com/player5_fragment/'+url+'.'+url+'?autoplay=true&autostart=true&il=en', headers=request_headers)
		#response = urllib2.urlopen(req)
		#data=response.read()
		#response.close()
		#matchs = re.compile('x-mpegURL" src="(.+?)">').findall(data)
		#for rr in matchs:
		#    stream = rr #.replace('https://0.viki.io','https://content.viki.com')
		
		
		if (debug == 'false' and rapi == 'true'):  
			#Заявка за получаване на видео стрийма
			req = urllib2.Request(SIGN(url,'/streams.json'))
			req.add_header('User-Agent', UA)
			opener = urllib2.build_opener()
			f = opener.open(req)
			jsonrsp = json.loads(f.read())
			#print SIGN(url,'/streams.json')
			#print jsonrsp
			
			#Избрано качество от потребителя
			if '360p' in jsonrsp:
				if (quality == '3' or quality == '2'): #MPEG-DASH DYNAMIC MOBILE
					stream = jsonrsp['mpd']['http']['url'] #.replace('https','http')
					xbmc.executebuiltin("Notification(VIKI®,Dynamic Mobile Streaming: MPEG-DASH,2000)")
				#elif quality == '2': #MPEG-DASH 480p
					#stream = jsonrsp['480p']['https']['url'].replace('https','http').replace('v4.viki.io','http')
					#xbmc.executebuiltin("Notification(VIKI®,DASH Streaming: 480p,2000)")
				#elif quality == '1': #Direct MP4 360p
					#domain = re.search('//(.+?)/', jsonrsp['360p']['https']['url']).group(1)
					#stream = jsonrsp['360p']['https']['url'].replace(domain,'content.viki.com').replace('https','http')
					#xbmc.executebuiltin("Notification(VIKI®,MP4 Progressive Download,2000)")
				else: #Direct MP4 480p
					#domain = re.search('//(.+?)/', jsonrsp['480p']['https']['url']).group(1)
					stream = jsonrsp['480p']['https']['url'] #.replace(domain,'content.viki.com') #.replace('https','http')
					xbmc.executebuiltin("Notification(VIKI®,MP4 Progressive Download,2000)")
					
			else:
				xbmc.executebuiltin("Notification(VIKI®,The addon needs rewriting!!!,2000)")

		#Ако е разрешен дебъга
		else:
			xbmc.executebuiltin("Notification(VIKI®,Debug mode is no longer supported <!>,8000)")
		
		
		#Зареждане на видеото
		try:
			if 'dash' not in stream:
				stream = stream+'|verifypeer=false&User-Agent='+urllib.quote_plus(MUA)+'&Referer=https://www.viki.com'
			if 'dash' in stream:
				li.setProperty('inputstreamaddon', 'inputstream.adaptive')
				li.setProperty('inputstream.adaptive.manifest_type', 'mpd')
				li.setProperty('inputstream.adaptive.stream_headers', 'verifypeer=false&User-Agent='+urllib.quote_plus(UA)+'&Referer=https://www.viki.com')
			#print stream
			li = xbmcgui.ListItem(iconImage=thumbnail, thumbnailImage=thumbnail, path=stream)
			li.setInfo( type="Video", infoLabels={ "Title": name, "Plot": plot } )
			xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, li)
			
			#Задаване на субтитри, ако има такива или изключването им
			if sub=='true':
				while not xbmc.Player().isPlaying():
					xbmc.sleep(1000) #wait until video is being played
					xbmc.Player().setSubtitles(srtsubs_path)
			else:
				xbmc.Player().showSubtitles(False)
		except:
			xbmc.executebuiltin("Notification(VIKI®,Addon cannot play this Video <R>,2000)")





#Модул за добавяне на отделно заглавие и неговите атрибути към съдържанието на показваната в Kodi директория - НЯМА НУЖДА ДА ПРОМЕНЯТЕ НИЩО ТУК
def addLink(name,url,vd,hd,plot,author,rating,ar,mode,iconimage):
		u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
		ok=True
		liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
		liz.setArt({ 'thumb': iconimage,'poster': iconimage, 'banner' : iconimage, 'fanart': iconimage })
		liz.setInfo( type="Video", infoLabels={ "Title": name, "Rating": ar } )
		liz.setInfo( type="Video", infoLabels={ "Duration": vd, "Plot": plot } )
		#liz.setInfo( type="Video", infoLabels={ "PlotOutline": "Това е plotoutline", "Tagline": "Това е tagline" } )
		liz.setInfo( type="Video", infoLabels={ "Studio": author, "Mpaa": rating } )
		if hd=='True':
			liz.addStreamInfo('video', { 'width': 1280, 'height': 720 })
			liz.addStreamInfo('video', { 'aspect': 1.78, 'codec': 'h264' })
		else:
			liz.addStreamInfo('video', { 'width': 720, 'height': 480 })
			liz.addStreamInfo('video', { 'aspect': 1.5, 'codec': 'h264' })
		liz.addStreamInfo('audio', { 'codec': 'aac', 'channels': 2 })
		liz.setProperty("IsPlayable" , "true")
		
		contextmenu = []
		contextmenu.append(('Information', 'XBMC.Action(Info)'))
		liz.addContextMenuItems(contextmenu)
		
		ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
		return ok

#Модул за добавяне на отделна директория и нейните атрибути към съдържанието на показваната в Kodi директория - НЯМА НУЖДА ДА ПРОМЕНЯТЕ НИЩО ТУК
def addDir(name,url,plot,mode,iconimage):
		u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
		ok=True
		liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
		liz.setInfo( type="Video", infoLabels={ "Title": name, "Plot": plot } )
		
		if len(plot)>0:
			contextmenu = []
			contextmenu.append(('Information', 'XBMC.Action(Info)'))
			liz.addContextMenuItems(contextmenu)
		
		ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
		return ok


#НЯМА НУЖДА ДА ПРОМЕНЯТЕ НИЩО ТУК
def get_params():
		param=[]
		paramstring=sys.argv[2]
		if len(paramstring)>=2:
				params=sys.argv[2]
				cleanedparams=params.replace('?','')
				if (params[len(params)-1]=='/'):
						params=params[0:len(params)-2]
				pairsofparams=cleanedparams.split('&')
				param={}
				for i in range(len(pairsofparams)):
						splitparams={}
						splitparams=pairsofparams[i].split('=')
						if (len(splitparams))==2:
								param[splitparams[0]]=splitparams[1]
								
		return param



params=get_params()
url=None
name=None
iconimage=None
mode=None

try:
		url=urllib.unquote_plus(params["url"])
except:
		pass
try:
		name=urllib.unquote_plus(params["name"])
except:
		pass
try:
		name=urllib.unquote_plus(params["iconimage"])
except:
		pass
try:
		mode=int(params["mode"])
except:
		pass


#Списък на отделните подпрограми/модули в тази приставка - трябва напълно да отговаря на кода отгоре
if mode==None or url==None or len(url)<1:
		CATEGORIES()
	
elif mode==1:
		INDEX(url)

elif mode==2:
		PREPARE(url)

elif mode==3:
		SEARCH(url)

elif mode==4:
		PLAY(name,url,iconimage)

elif mode==5:
		SIGN(url,pth)

elif mode==6:
		GENRE(url)

elif mode==7:
		COUNTRY(url)

elif mode==8:
		LOADBYID()


xbmcplugin.endOfDirectory(int(sys.argv[1]))
