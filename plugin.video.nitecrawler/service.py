# -*- coding: utf-8 -*-

from resources.lib.modules import control,log_utils

control.execute('RunPlugin(plugin://%s)' % control.get_plugin_url({'action': 'service'}))

try:
    AddonVersion = control.addon('plugin.video.nitecrawler').getAddonInfo('version')
    ModuleVersion = control.addon('script.module.universalscrapers').getAddonInfo('version')
    RepoVersion = control.addon('repository.jewrepo').getAddonInfo('version')
    log_utils.log('===-[AddonVersion: %s]-' % str(AddonVersion) + '-[ModuleVersion: %s]-' % str(ModuleVersion) + '-[RepoVersion: %s]-' % str(RepoVersion), log_utils.LOGNOTICE)
except:
    log_utils.log('===-[Error Oppps...', log_utils.LOGNOTICE)
    log_utils.log('===-[Had Trouble Getting Version Info. Make Sure You Have the JewRepo.', log_utils.LOGNOTICE)
