# -*- coding: utf-8 -*-

import os, base64, sys, urllib2, urlparse
import xbmc, xbmcaddon, xbmcgui
from resources.lib.modules import control, cache, trakt

sysaddon = sys.argv[0] ; syshandle = int(sys.argv[1]) ; control.moderator()
artPath = control.artPath() ; addonFanart = control.addonFanart()
imdbCredentials = False if control.setting('imdb.user') == '' else True
traktCredentials = trakt.getTraktCredentialsInfo()
traktIndicators = trakt.getTraktIndicatorsInfo()
queueMenu = control.lang(32065).encode('utf-8')


class navigator:
    def getMenuEnabled(self, menu_title):
        is_enabled = control.setting(menu_title).strip()
        if (is_enabled == '' or is_enabled == 'false'):
            return False
        return True


    def root(self):
        self.addDirectoryItem(32001, 'movieNavigator', 'movies.png', 'DefaultMovies.png')
        self.addDirectoryItem(32002, 'tvNavigator', 'tvshows.png', 'DefaultTVShows.png')
        if not control.setting('lists.widget') == '0':
            self.addDirectoryItem(32003, 'mymovieNavigator', 'mymovies.png', 'DefaultSets.png')
            self.addDirectoryItem(32004, 'mytvNavigator', 'mytvshows.png', 'DefaultSets.png')
        self.addDirectoryItem(32005, 'movieWidget', 'latest-movies.png', 'DefaultRecentlyAddedMovies.png')
        self.addDirectoryItem(32006, 'tvWidget', 'latest-episodes.png', 'DefaultRecentlyAddedEpisodes.png')
        self.addDirectoryItem('IMDb Lists', 'imdbLists', 'imdb.png', 'DefaultVideoPlaylists.png')
        self.addDirectoryItem('Sky Channels', 'channels', 'channels.png', 'DefaultFolder.png')
        self.addDirectoryItem('More Stuff', 'moreplugs', 'movies.png', 'DefaultFolder.png')
        downloads = True if control.setting('downloads') == 'true' and (len(control.listDir(control.setting('movie.download.path'))[0]) > 0 or len(control.listDir(control.setting('tv.download.path'))[0]) > 0) else False
        if downloads == True:
            self.addDirectoryItem(32009, 'downloadNavigator', 'downloads.png', 'DefaultFolder.png')
        self.addDirectoryItem('Search Em', 'searchNavigator', 'search.png', 'DefaultAddonsSearch.png') #32010
        self.addDirectoryItem(32008, 'toolNavigator', 'tools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('Show Changelog',  'ShowChangelog',  'icon.png',  'DefaultFolder.png')
        if self.getMenuEnabled('navi.dev') == True:
            self.addDirectoryItem('Dev Menu', 'devtoolNavigator', 'tools.png', 'DefaultAddonProgram.png')
        self.endDirectory()


    def movies(self, lite=False):
        self.addDirectoryItem('Popular', 'movies&url=popular', 'imdb.png', 'DefaultMovies.png')
        self.addDirectoryItem('Trending', 'movies&url=trending', 'trakt.png', 'DefaultMovies.png')
        self.addDirectoryItem('Anticipated', 'movies&url=anticipated', 'latest-movies.png', 'DefaultMovies.png')
        self.addDirectoryItem(32035, 'movies&url=featured', 'featured.png', 'DefaultMovies.png')
        self.addDirectoryItem(32022, 'movies&url=theaters', 'in-theaters.png', 'DefaultRecentlyAddedMovies.png')
        self.addDirectoryItem(32020, 'movies&url=boxoffice', 'box-office.png', 'DefaultMovies.png')
        self.addDirectoryItem('Box Office2', 'movies&url=boxoffice2', 'box-office.png', 'DefaultMovies.png')
        self.addDirectoryItem(32021, 'movies&url=oscars', 'oscar-winners.png', 'DefaultMovies.png')
        self.addDirectoryItem('Movie Mosts', 'movieMosts', 'people-watching.png', 'DefaultMovies.png')
        self.addDirectoryItem(32011, 'movieGenres', 'genres.png', 'DefaultMovies.png')
        self.addDirectoryItem(32012, 'movieYears', 'years.png', 'DefaultMovies.png')
        self.addDirectoryItem('Actor', 'moviePersons', 'actor.png', 'DefaultMovies.png')
        self.addDirectoryItem(32014, 'movieLanguages', 'languages.png', 'DefaultMovies.png')
        self.addDirectoryItem(32015, 'movieCertificates', 'certificates.png', 'DefaultMovies.png')
        if lite == False:
            if not control.setting('lists.widget') == '0':
                self.addDirectoryItem(32003, 'mymovieliteNavigator', 'mymovies.png', 'DefaultVideoPlaylists.png')
            self.addDirectoryItem('Search Em', 'searchNavigator', 'search.png', 'DefaultAddonsSearch.png') #32010
        self.endDirectory()


    def mymovies(self, lite=False):
        self.accountCheck()
        if traktCredentials == True and imdbCredentials == True:
            self.addDirectoryItem(32032, 'movies&url=traktcollection', 'trakt.png', 'DefaultMovies.png', queue=True, context=(32551, 'moviesToLibrary&url=traktcollection'))
            self.addDirectoryItem(32033, 'movies&url=traktwatchlist', 'trakt.png', 'DefaultMovies.png', queue=True, context=(32551, 'moviesToLibrary&url=traktwatchlist'))
            self.addDirectoryItem(32034, 'movies&url=imdbwatchlist', 'imdb.png', 'DefaultMovies.png', queue=True)
        elif traktCredentials == True:
            self.addDirectoryItem(32032, 'movies&url=traktcollection', 'trakt.png', 'DefaultMovies.png', queue=True, context=(32551, 'moviesToLibrary&url=traktcollection'))
            self.addDirectoryItem(32033, 'movies&url=traktwatchlist', 'trakt.png', 'DefaultMovies.png', queue=True, context=(32551, 'moviesToLibrary&url=traktwatchlist'))
        elif imdbCredentials == True:
            self.addDirectoryItem(32032, 'movies&url=imdbwatchlist', 'imdb.png', 'DefaultMovies.png', queue=True)
            self.addDirectoryItem(32033, 'movies&url=imdbwatchlist2', 'imdb.png', 'DefaultMovies.png', queue=True)
        if traktIndicators == True:
            self.addDirectoryItem(32036, 'movies&url=trakthistory', 'trakt.png', 'DefaultMovies.png', queue=True)
        self.addDirectoryItem(32039, 'movieUserlists', 'mymovies.png', 'DefaultMovies.png')
        if lite == False:
            self.addDirectoryItem(32031, 'movieliteNavigator', 'movies.png', 'DefaultMovies.png')
        self.endDirectory()


    def movieMosts(self):
        self.addDirectoryItem(32019, 'movies&url=views', 'most-voted.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Played This Week', 'movies&url=played1', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Played This Month', 'movies&url=played2', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Played This Year', 'movies&url=played3', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Played All Time', 'movies&url=played4', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Collected This Week', 'movies&url=collected1', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Collected This Month', 'movies&url=collected2', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Collected This Year', 'movies&url=collected3', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Collected All Time', 'movies&url=collected4', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Watched This Week', 'movies&url=watched1', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Watched This Month', 'movies&url=watched2', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Watched This Year', 'movies&url=watched3', 'mosts.png', 'DefaultMovies.png')
        self.addDirectoryItem('Most Watched All Time', 'movies&url=watched4', 'mosts.png', 'DefaultMovies.png')
        self.endDirectory()	


    def tvshows(self, lite=False):
        self.addDirectoryItem('Popular', 'tvshows&url=popular', 'imdb.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32035, 'tvshows&url=traktfeatured', 'featured.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Trending', 'tvshows&url=trending', 'trakt.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Anticipated', 'tvshows&url=anticipated', 'trakt.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Show Premieres', 'tvshows&url=premieres', 'trakt.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32026, 'tvshows&url=premiere', 'new-tvshows.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32024, 'tvshows&url=airing', 'airing-today.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32025, 'tvshows&url=active', 'returning-tvshows.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32006, 'calendar&url=added', 'latest-episodes.png', 'DefaultRecentlyAddedEpisodes.png', queue=True)
        self.addDirectoryItem(32027, 'calendars', 'calendar.png', 'DefaultRecentlyAddedEpisodes.png')
        self.addDirectoryItem(32023, 'tvshows&url=rating', 'most.png', 'DefaultTVShows.png')
        self.addDirectoryItem('TV Show Mosts', 'showMosts', 'people-watching.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32011, 'tvGenres', 'genres.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32016, 'tvNetworksNavigator', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32014, 'tvLanguages', 'languages.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32015, 'tvCertificates', 'certificates.png', 'DefaultTVShows.png')
        if lite == False:
            if not control.setting('lists.widget') == '0':
                self.addDirectoryItem(32004, 'mytvliteNavigator', 'mytvshows.png', 'DefaultVideoPlaylists.png')
            self.addDirectoryItem('Search Em', 'searchNavigator', 'search.png', 'DefaultAddonsSearch.png') #32010
        self.endDirectory()


    def mytvshows(self, lite=False):
        self.accountCheck()
        if traktCredentials == True and imdbCredentials == True:
            self.addDirectoryItem(32032, 'tvshows&url=traktcollection', 'trakt.png', 'DefaultTVShows.png', context=(32551, 'tvshowsToLibrary&url=traktcollection'))
            self.addDirectoryItem(32033, 'tvshows&url=traktwatchlist', 'trakt.png', 'DefaultTVShows.png', context=(32551, 'tvshowsToLibrary&url=traktwatchlist'))
            self.addDirectoryItem(32034, 'tvshows&url=imdbwatchlist', 'imdb.png', 'DefaultTVShows.png')
        elif traktCredentials == True:
            self.addDirectoryItem(32032, 'tvshows&url=traktcollection', 'trakt.png', 'DefaultTVShows.png', context=(32551, 'tvshowsToLibrary&url=traktcollection'))
            self.addDirectoryItem(32033, 'tvshows&url=traktwatchlist', 'trakt.png', 'DefaultTVShows.png', context=(32551, 'tvshowsToLibrary&url=traktwatchlist'))
        elif imdbCredentials == True:
            self.addDirectoryItem(32032, 'tvshows&url=imdbwatchlist', 'imdb.png', 'DefaultTVShows.png')
            self.addDirectoryItem(32033, 'tvshows&url=imdbwatchlist2', 'imdb.png', 'DefaultTVShows.png')
        if traktIndicators == True:
            self.addDirectoryItem(32036, 'calendar&url=trakthistory', 'trakt.png', 'DefaultTVShows.png', queue=True)
            self.addDirectoryItem(32037, 'calendar&url=progress', 'trakt.png', 'DefaultRecentlyAddedEpisodes.png', queue=True)
            self.addDirectoryItem(32038, 'calendar&url=mycalendar', 'trakt.png', 'DefaultRecentlyAddedEpisodes.png', queue=True)
        self.addDirectoryItem(32040, 'tvUserlists', 'mytvshows.png', 'DefaultTVShows.png')
        if traktCredentials == True:
            self.addDirectoryItem(32041, 'episodeUserlists', 'mytvshows.png', 'DefaultTVShows.png')
        if lite == False:
            self.addDirectoryItem(32031, 'tvliteNavigator', 'tvshows.png', 'DefaultTVShows.png')
        self.endDirectory()


    def showMosts(self):
        self.addDirectoryItem(32019, 'tvshows&url=views', 'most-voted.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Played This Week', 'tvshows&url=played1', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Played This Month', 'tvshows&url=played2', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Played This Year', 'tvshows&url=played3', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Played All Time', 'tvshows&url=played4', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Collected This Week', 'tvshows&url=collected1', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Collected This Month', 'tvshows&url=collected2', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Collected This Year', 'tvshows&url=collected3', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Collected All Time', 'tvshows&url=collected4', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Watched This Week', 'tvshows&url=watched1', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Watched This Month', 'tvshows&url=watched2', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Watched This Year', 'tvshows&url=watched3', 'mosts.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Most Watched All Time', 'tvshows&url=watched4', 'mosts.png', 'DefaultTVShows.png')
        self.endDirectory()


    def tvNetworksNavigator(self):
        self.addDirectoryItem('WebChannels', 'tvWebChannels', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('United States', 'tvNetworks', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Canada', 'tvCanadanetworks', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('United Kingdom', 'tvUnitedKingdomnetworks', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Australia', 'tvAustralianetworks', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Other Countries 1', 'tvOthers1networks', 'networks.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Other Countries 2', 'tvOthers2networks', 'networks.png', 'DefaultTVShows.png')
        self.endDirectory()


    def imdbLists(self):
        self.addDirectoryItem('Explore Keywords(Movies)', 'movieExploreKeywords', 'imdb.png', 'DefaultMovies.png')
        self.addDirectoryItem('Explore Keywords(TV Shows)', 'tvshowExploreKeywords', 'imdb.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Explore UserLists(Movies)', 'movieimdbUserLists', 'imdb.png', 'DefaultMovies.png')
        self.addDirectoryItem('Explore UserLists(TV Shows)', 'tvshowimdbUserLists', 'imdb.png', 'DefaultTVShows.png')
        self.endDirectory()


    def moreplugs(self):
        self.addDirectoryItem('IPTV', 'iptvChannels', 'channels.png', 'DefaultTVShows.png')
        self.addDirectoryItem('MusicChoice', 'jewMC', 'most-voted.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Tunes', 'radios', 'most-voted.png', 'DefaultTVShows.png')
        self.addDirectoryItem(32610, 'kidscorner', 'most-voted.png', 'DefaultVideoPlaylists.png')
        self.addDirectoryItem(32611, 'fitness', 'most-voted.png', 'DefaultVideoPlaylists.png')
        self.addDirectoryItem(326232, 'tvReviews', 'most-voted.png', 'DefaultVideoPlaylists.png')
        self.addDirectoryItem(32623, 'movieReviews', 'most-voted.png', 'DefaultVideoPlaylists.png')
        if self.getMenuEnabled('navi.xxx') == True:
            self.addDirectoryItem('[COLOR black]xxx[/COLOR]', 'navXXX', 'most-voted.png', 'DefaultTVShows.png')
        self.endDirectory()


    def tools(self):
        self.addDirectoryItem(32043, 'openSettings&query=0.0', 'Settings.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32556, 'libraryNavigator', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32049, 'viewsNavigator', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32050, 'clearSources', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32052, 'clearCache', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('Scraper Settings', 'nanscrapersettings', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(42002, 'urlResolver', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(42001, 'authTrakt', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('Pair Em',  'PairEm',  'tools.png',  'DefaultAddonProgram.png')
        self.endDirectory()


    def devtools(self):
        self.addDirectoryItem('Search Em', 'searchNavigator', 'search.png', 'DefaultAddonsSearch.png')
        self.addDirectoryItem(32022, 'movies&url=theaters', 'in-theaters.png', 'DefaultRecentlyAddedMovies.png')
        self.addDirectoryItem(32006, 'calendar&url=added', 'latest-episodes.png', 'DefaultRecentlyAddedEpisodes.png', queue=True)
        self.addDirectoryItem(32052, 'clearCache', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32050, 'clearSources', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('Clear ResolveURL Cache', 'clearResolveURLcache', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('Scraper Settings', 'nanscrapersettings', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem('ResolveURL Settings', 'urlResolver', 'Quicktools.png', 'DefaultAddonProgram.png')
        self.endDirectory()


    def library(self):
        self.addDirectoryItem(32557, 'openSettings&query=4.0', 'Settings.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32558, 'updateLibrary&query=tool', 'library_update.png', 'DefaultAddonProgram.png')
        self.addDirectoryItem(32559, control.setting('library.movie'), 'movies.png', 'DefaultMovies.png', isAction=False)
        self.addDirectoryItem(32560, control.setting('library.tv'), 'tvshows.png', 'DefaultTVShows.png', isAction=False)
        if trakt.getTraktCredentialsInfo():
            self.addDirectoryItem(32561, 'moviesToLibrary&url=traktcollection', 'trakt.png', 'DefaultMovies.png')
            self.addDirectoryItem(32562, 'moviesToLibrary&url=traktwatchlist', 'trakt.png', 'DefaultMovies.png')
            self.addDirectoryItem(32563, 'tvshowsToLibrary&url=traktcollection', 'trakt.png', 'DefaultTVShows.png')
            self.addDirectoryItem(32564, 'tvshowsToLibrary&url=traktwatchlist', 'trakt.png', 'DefaultTVShows.png')
        self.endDirectory()


    def downloads(self):
        movie_downloads = control.setting('movie.download.path')
        tv_downloads = control.setting('tv.download.path')
        if len(control.listDir(movie_downloads)[0]) > 0:
            self.addDirectoryItem(32001, movie_downloads, 'movies.png', 'DefaultMovies.png', isAction=False)
        if len(control.listDir(tv_downloads)[0]) > 0:
            self.addDirectoryItem(32002, tv_downloads, 'tvshows.png', 'DefaultTVShows.png', isAction=False)
        self.endDirectory()


    def search(self):
        self.addDirectoryItem(32001, 'movieSearch', 'search.png', 'DefaultMovies.png')
        self.addDirectoryItem(32002, 'tvSearch', 'search.png', 'DefaultTVShows.png')
        self.addDirectoryItem('Search (Actor)', 'moviePerson', 'actorsearch.png', 'DefaultMovies.png')
        self.addDirectoryItem('Search (TV Actor)', 'tvPerson', 'actorsearch.png', 'DefaultTVShows.png')
        self.endDirectory()


    def views(self):
        try:
            control.idle()
            items = [(control.lang(32001).encode('utf-8'), 'movies'), (control.lang(32002).encode('utf-8'), 'tvshows'),
                    (control.lang(32054).encode('utf-8'), 'seasons'), (control.lang(32038).encode('utf-8'), 'episodes')]
            select = control.selectDialog([i[0] for i in items], control.lang(32049).encode('utf-8'))
            if select == -1:
                return
            content = items[select][1]
            title = control.lang(32059).encode('utf-8')
            url = '%s?action=addView&content=%s' % (sys.argv[0], content)
            poster, banner, fanart = control.addonPoster(), control.addonBanner(), control.addonFanart()
            item = control.item(label=title)
            item.setInfo(type='Video', infoLabels={'title': title})
            item.setArt({'icon': poster, 'thumb': poster, 'poster': poster, 'banner': banner})
            item.setProperty('Fanart_Image', fanart)
            control.addItem(handle=int(sys.argv[1]), url=url, listitem=item, isFolder=False)
            control.content(int(sys.argv[1]), content)
            control.directory(int(sys.argv[1]), cacheToDisc=True)
            from resources.lib.modules import views
            views.setView(content, {})
        except:
            return


    def accountCheck(self):
        if traktCredentials == False and imdbCredentials == False:
            control.idle()
            control.infoDialog(control.lang(32042).encode('utf-8'), sound=True, icon='WARNING')
            sys.exit()


    def clearCache(self):
        control.idle()
        yes = control.yesnoDialog(control.lang(32056).encode('utf-8'), '', '')
        if not yes:
            return
        from resources.lib.modules import cache
        cache.cache_clear()
        control.infoDialog(control.lang(32057).encode('utf-8'), sound=True, icon='INFO')


    def clearCacheSearch(self):
        control.idle()
        yes = control.yesnoDialog(control.lang(32056).encode('utf-8'), '', '')
        if not yes:
            return
        from resources.lib.modules import cache
        cache.cache_clear_search()
        control.infoDialog(control.lang(32057).encode('utf-8'), sound=True, icon='INFO')


    def addDirectoryItem(self, name, query, thumb, icon, context=None, queue=False, isAction=True, isFolder=True):
        try:
            name = control.lang(name).encode('utf-8')
        except:
            pass
        url = '%s?action=%s' % (sysaddon, query) if isAction == True else query
        thumb = os.path.join(artPath, thumb) if not artPath == None else icon
        cm = []
        if queue == True:
            cm.append((queueMenu, 'RunPlugin(%s?action=queueItem)' % sysaddon))
        if not context == None:
            cm.append((control.lang(context[0]).encode('utf-8'), 'RunPlugin(%s?action=%s)' % (sysaddon, context[1])))
        item = control.item(label=name)
        item.addContextMenuItems(cm)
        item.setArt({'icon': thumb, 'thumb': thumb})
        if not addonFanart == None:
            item.setProperty('Fanart_Image', addonFanart)
        control.addItem(handle=syshandle, url=url, listitem=item, isFolder=isFolder)


    def endDirectory(self):
        control.content(syshandle, 'addons')
        control.directory(syshandle, cacheToDisc=True)


